# Produkt Management

[[_TOC_]]


## Projektidee
Wir wollen ein Programm schreiben, dass Kunden verwaltet.
Man soll Kunden hinzufügen, bearbeiten und löschen können.
Das Programm ist mit Jdbc an eine Mysql Datenbank angehängt.


## Datenbank
CREATE TABLE IF NOT EXISTS `customer` (<br>
`id` int(11) NOT NULL AUTO_INCREMENT,<br>
`firstname` varchar(255) NOT NULL,<br>
`lastname` varchar(255) NOT NULL,<br>
`email` varchar(255) NOT NULL,<br>
`message` varchar(255) NOT NULL,<br>
PRIMARY KEY (`id`)<br>
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


## Wireframe
![Wireframe](wireframe.png "Wireframe")


## USE-Case Diagramm
![UseCaseDiagramm](UseCase.png "UseCaseDiagramm")


## Dokumentation


### 22.06.2021
Wir haben das Wireframe sowie das Use-Case Diagramm gemacht.<br>
Das Projekt hat noch überhaupt nicht funktioniert, aber wir haben das Projekt erstellt.


### 29.06.2021
Wir haben das Beispiel von Cavuoti zum Laufen gebracht.


### 06.07.2021
Wir haben die Fragments und die Internationalisierung hinzugefügt. 


© Widmer Furrer
