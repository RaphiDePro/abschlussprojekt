package ch.schule.ecustomer;

import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


//@Named
//@Singleton
//@RequestScoped
//@Repository
//@SessionScope
@Repository
public class CustomerDao implements Serializable {

    private static final Logger log = Logger.getLogger(CustomerDao.class.getSimpleName());


    // Die Datenbank connection
    private Connection connection;

    @PostConstruct
    private void init() {
        log.info("-------- MySQL JDBC Connection Testing ------------");

        try {
            // Treiber-Klasse Laden
            Class.forName("com.mysql.jdbc.Driver");
            log.info("the driver is loaded");
        } catch (ClassNotFoundException e) {
            log.info("Where is your MySQL JDBC Driver?");
            e.printStackTrace();

        }

        log.info("MySQL JDBC Driver Registered!");
        connection = null;

        try {
            // Verbindung aufbauen
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_customer", "root", "");

        } catch (SQLException e) {
            log.info("Connection Failed! Check output console");
            e.printStackTrace();

        }

        if (connection != null) {
            log.info("You made it, take control your database now!");
        } else {
            log.info("Failed to make connection!");

        }
    }

    public void updateCustomer(Customer costumer){
        PreparedStatement pst = null;
        String stm = "UPDATE customer set firstname = ? , lastname = ? , email = ? , message = ? where id = ?";

        try {
            pst = connection.prepareStatement(stm);
            pst.setString(1, costumer.getFirstname());
            pst.setString(2, costumer.getLastname());
            pst.setString(3, costumer.getEmail());
            pst.setString(4, costumer.getMessage());
            pst.setInt(5, costumer.getId());
            pst.execute();


            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addCustomer(Customer costumer){
        PreparedStatement pst = null;
        String stm = "INSERT into customer(firstname, lastname, email, message) values (?,?,?,?)";

        try {
            pst = connection.prepareStatement(stm);
            pst.setString(1, costumer.getFirstname());
            pst.setString(2, costumer.getLastname());
            pst.setString(3, costumer.getEmail());
            pst.setString(4, costumer.getMessage());
            pst.execute();


            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCustomer(int id){
        PreparedStatement pst = null;
        String stm = "delete from customer where id = ?";

        try {
            pst = connection.prepareStatement(stm);
            pst.setInt(1, id);
            pst.execute();


            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return Customer
     */
    public Customer getCustomer(int id) {
        Customer customer = new Customer();
        // Hier kommt die Verarbeitung der Datenbank auslesen und anzeigen
        ResultSet rs = null;
        PreparedStatement pst = null;
        String stm = "Select * from customer where id = ?";

        try {
            pst = connection.prepareStatement(stm);
            pst.setInt(1, id);
            pst.execute();
            rs = pst.getResultSet();

            while (rs.next()) {
                customer.setId( rs.getInt(1) );
                customer.setFirsName( rs.getString("firstname") );
                customer.setLastname( rs.getString(3) );
                customer.setEmail( rs.getString(4) );
                customer.setMessage( rs.getString(5) );
                log.info("customer: " + customer);
            }
            // alle objecte schliessen
            pst.close();
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * @return List<Customer>
     */
    public List<Customer> getCustomers() {
        // Die Liste der Kunden
        List<Customer> customers = new ArrayList<Customer>();
        // Hier kommt die Verarbeitung der Datenbank auslesen und anzeigen
        ResultSet rs = null;
        PreparedStatement pst = null;
        String stm = "Select * from customer";

        try {
            pst = connection.prepareStatement(stm);
            pst.execute();
            rs = pst.getResultSet();

            while (rs.next()) {
                Customer customer = new Customer();
                customer.setId( rs.getInt(1) );
                customer.setFirsName( rs.getString("firstname") );
                customer.setLastname( rs.getString(3) );
                customer.setEmail( rs.getString(4) );
                customer.setMessage( rs.getString(5) );
                log.info("customer: " + customer);
                customers.add(customer);
            }
            // alle objecte schliessen
            pst.close();
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    @PreDestroy
    public void destroy() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
