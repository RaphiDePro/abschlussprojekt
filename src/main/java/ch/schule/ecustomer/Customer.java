package ch.schule.ecustomer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * 
 * @author luigicavuoti
 * Das Modell
 */
public class Customer {

	private int id;
	@NotEmpty
	@Size(min = 1, max = 255)
	private String firstname;
	@NotEmpty
	@Size(min = 1, max = 255)
	private String lastname;
	@NotEmpty
	@Size(min = 1, max = 255)
	private String email;
	@Size(min = 0, max = 255)
	private String message;

	public Customer() {
		this.firstname = "";
		this.lastname = "";
	}

	public Customer(String firstname, String lastName, String email, String message) {
		this.firstname = firstname;
		this.lastname = lastName;
		this.email=email;
		this.message=message;

	}
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirsName(String firsName) {
		this.firstname = firsName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstname + ", lastName=" + lastname + ", email=" + email
				+ ", message=" + message + ", hashcode="+this.hashCode()+"]";
	}
	
	
}
