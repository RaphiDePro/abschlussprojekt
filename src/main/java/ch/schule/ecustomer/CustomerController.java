package ch.schule.ecustomer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.logging.Logger;

@Controller
//@RequestScope
public class CustomerController implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(CustomerDao.class.getSimpleName());

    @Autowired
    private final CustomerDao customerDao;

    public CustomerController(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }


    @PostConstruct
    public void init() {
        log.info("im init");
    }

    @GetMapping("/")
    public String getHome() {
        return "redirect:/customers";
    }


    @GetMapping("/customers")
    public String getCustomers(Model model) {
        model.addAttribute("customers", customerDao.getCustomers()); // Spring - DI
        log.info("List " + customerDao.getCustomers());
        model.addAttribute("customer", new Customer());
        return "customers";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable int id, Model model) {
        Customer customer = customerDao.getCustomer(id);
        model.addAttribute("customer", customer);
        return "add";
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("customer", new Customer());
        return "add";
    }

    @PostMapping("/save")
    public String save(@Valid @ModelAttribute Customer customer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "add";
        }
        if (customer.getId() != 0) {
            customerDao.updateCustomer(customer);
        } else {
            customerDao.addCustomer(customer);
        }
        return "redirect:/";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable int id) {
        customerDao.deleteCustomer(id);
        return "redirect:/";
    }
}
